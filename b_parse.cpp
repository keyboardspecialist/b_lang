#include "b_parse.h"


void
BParser::genAST(void)
{
  LOG_MSG("\nBeginning AST Generation\n");

  curToken = blex->m_context->m_tokQueue.begin();
  while(1)
    {
      switch(curToken->type)
	{
	case BSymbol::kNull:
	  LOG_MSG("\nEnd Of File reached\n");
	  return;
	  break;

	  //function, extrn var
	case BSymbol::kIdent:
	  parseExtrn();
	  break;
	default:
	  curToken++;
	}
    }
  
}

void
BParser::parseExtrn(void)
{
  BString identName = curToken->token; //gah
  ExtrnDeclAST* node;
  curToken++;
  if(curToken->type == BSymbol::kLParen)
    {
      node = new FuncDeclAST();
      node->extrnName = identName;
      parseFuncExtrn((FuncDeclAST*)node);
      
      return;
    }
  else
    {
      node = new ExtrnVarDeclAST();
      node->extrnName = identName;
      parseVarExtrn((ExtrnVarDeclAST*)node);

      return;
    }
}

void
BParser::parseVarExtrn(ExtrnVarDeclAST* node)
{
  LOG_MSG(">>Parsing Extrn Var Decl %s\n", node->extrnName.c_str());

  //first check if we have a vector decl
  if(curToken->type == BSymbol::kLBrak)
    {
      LOG_MSG("Extrn '%s' declared as vector\n", node->extrnName.c_str());
      curToken++;
      if(curToken->type == BSymbol::kWord)
	{
	  int vsize = atoi(curToken->token.c_str());
	  LOG_MSG("Extrn Decl size %d\n", vsize);
	  if(vsize < 0)
	    {
	      vsize = +vsize;
	      LOG_MSG("Negative value used for vector allocation on '%s', allocating [%d]. Did you mean positive?\n", node->extrnName.c_str(), vsize);
	    }
	   
	  node->vectorAllocation = vsize;
	  
	  curToken++;
	  if(curToken->type != BSymbol::kRBrak)
	    {
	      LOG_MSG("Parse Error! Expected ']', but found %s for Extrn '%s'\n", curToken->token.c_str(), node->extrnName.c_str());
	      delete node;
	      node = NULL;
	      return;
	    }
	}
      else if(curToken->type == BSymbol::kRBrak)
	{
	  node->vectorAllocation = 0; //vector size is constant + 1
	}
      else
	{
	  LOG_MSG("Parse Error! Expected constant value or ']', but found %s for Extrn '%s'\n", curToken->token.c_str(), node->extrnName.c_str());
	  delete node;
	  node = NULL;
	  return;
	}

      curToken++;
    }
  else
    {
      //curToken++;
      node->vectorAllocation = -1; //not declared vector
    }

  while(1)
    {
      if(curToken->type == BSymbol::kWord)
	{
	  WordAST* wrd = new WordAST();
	  wrd->ival = (int64_t)atoll(curToken->token.c_str()); 
	  node->vals.push_back((ConstExprAST*)wrd);

	  LOG_MSG("Adding Word val to extrn '%s', %ld\n", node->extrnName.c_str(), wrd->ival);
	  curToken++;

	  if(curToken->type == BSymbol::kSColon)
	    continue;
	  if(curToken->type != BSymbol::kComma)
	    {
	      LOG_MSG("Parse Error! Expected ',' but found '%s'\n", curToken->token.c_str());
	      delete node;
	      node = NULL;
	      return;
	    }

	  curToken++;
	}
      else if(curToken->type == BSymbol::kChar)
	{
	  CharAST* cchr = new CharAST();
	  cchr->cval.ival = *((int64_t*)curToken->token.c_str());
	  node->vals.push_back((ConstExprAST*)cchr);

	  LOG_MSG("Adding Char val to extrn '%s', '%s'\n", node->extrnName.c_str(), cchr->cval.chars);
	  curToken++;

	  if(curToken->type == BSymbol::kSColon)
	    continue;
	  if(curToken->type != BSymbol::kComma)
	    {
	      LOG_MSG("Parse Error! Expected ',' but found '%s'\n", curToken->token.c_str());
	      delete node;
	      node = NULL;
	      return;
	    }

	  curToken++;
	}
      else if(curToken->type == BSymbol::kString)
	{
	  //Strings are vector decls by default. 
	  StringAST* cstr = new StringAST();
	  cstr->sval = curToken->token;
	  node->vals.push_back((ConstExprAST*)cstr);

	  LOG_MSG("Adding String val to extrn '%s', '%s'\n", node->extrnName.c_str(), cstr->sval.c_str());
	  curToken++;

	  if(curToken->type == BSymbol::kSColon)
	    continue;
	  if(curToken->type != BSymbol::kComma)
	    {
	      LOG_MSG("Parse Error! Expected ',' but found '%s'\n", curToken->token.c_str());
	      delete node;
	      node = NULL;
	      return;
	    }

	  curToken++;
	}
      else if(curToken->type == BSymbol::kIdent)
	{
	  NameAST* ident = new NameAST();
	  ident->name = curToken->token;
	  node->vals.push_back((ConstExprAST*)ident);

	  LOG_MSG("Adding Ident value to extrn '%s', %s\n", node->extrnName.c_str(), ident->name.c_str());
	  curToken++;

	  if(curToken->type == BSymbol::kSColon)
	    continue;
	  if(curToken->type != BSymbol::kComma)
	    {
	      LOG_MSG("Parse Error! Expected ',' but found '%s'\n", curToken->token.c_str());
	      delete node;
	      node = NULL;
	      return;
	    }

	  curToken++;
	}
      else if(curToken->type == BSymbol::kSColon)
	{
	  curToken++;
	  progAST->extrnDefs.push_back(node);
	  LOG_MSG("Semi-colon\n");
	  return;
	}
      else
	{
	  LOG_MSG("Parse Error! Bad token '%s' in Extrn Decl, [%s]\n", curToken->token.c_str(), node->extrnName.c_str());
	  delete node;
	  node = NULL;
	  return;
	}
       
    }
  
  return;
}

void
BParser::parseFuncExtrn(FuncDeclAST* node)
{
  LOG_MSG(">>Parsing FuncDecl %s\n", node->extrnName.c_str());

  //redundant now
  if(curToken->type != BSymbol::kLParen)
    {
      LOG_MSG("Parse error expected %c but found %s\n", TOK_LPAR, curToken->token.c_str());
      delete node;
      return;
    }

  curToken++;

  //parse arg list
  while(1)
    {
      if(curToken->type == BSymbol::kIdent)
	{
	  node->funcArgs.push_back(curToken->token);
	  LOG_MSG("Parsed Func Arg %s\n", curToken->token.c_str());
	  curToken++;
	}
      else if(curToken->type == BSymbol::kRParen)
	{
	  curToken++; //eat
	  progAST->extrnDefs.push_back(node);
	  //descend into body
	  
	  return;
	}
      else if(curToken->type == BSymbol::kComma)
	{
	  curToken++; //eat
	}
      else
	{
	  LOG_MSG("Parse Error! Expected ',' or ')' but found %s\n", curToken->token.c_str());
	  delete node;
	  node = NULL;
	  return;
	}
    }
}
  
