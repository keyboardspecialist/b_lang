#include "blang.h"



int
main(int argc, char** argv)
{
    if(argc < 2)
    {
        printf("No source files!");
        return 0;
    }

    BLexer blex;
    BParser bparse;
    
    for(int i = 1; i < argc; i++)
    {
        FILE* in = NULL;
        if((in = fopen(argv[i], "r")) != NULL)
        {
            blex.init(in);
            blex.genTokens();
            LOG_MSG("QUEUE SIZE:(main) %lu\n", blex.m_context->m_tokQueue.size());

	    bparse.init(std::make_shared<BLexer>(blex));
	    bparse.genAST();
	    
	    /*
            while(!blex.m_context->m_tokQueue.empty())
            {
                printf(">>> %s :: %i :: line %i\n", blex.m_context->m_tokQueue.front().token.c_str(),
                       blex.m_context->m_tokQueue.front().type,
                       blex.m_context->m_tokQueue.front().line);
                blex.m_context->m_tokQueue.pop();
            }
            */
        }
    }
}
