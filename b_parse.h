#pragma once


#define BLANG_LOGGING
#define BLANG_LOG_LEVEL 1

#include "b_lex.h"

struct ExtrnDeclAST
{
  virtual ~ExtrnDeclAST() {};

  BString extrnName;
};

struct FuncDeclAST : public ExtrnDeclAST
{
  virtual ~FuncDeclAST() {};

  std::vector< BString > funcArgs;
};

struct ConstExprAST
{
  virtual ~ConstExprAST() {};
};

struct NameAST : public ConstExprAST
{
  virtual ~NameAST() {};

  BString name;
};

struct WordAST : public ConstExprAST
{
  virtual ~WordAST() {};

  int64_t ival;
};

struct CharAST : public ConstExprAST
{
  virtual ~CharAST() {};

  //@TODO consider making this fundamental for byte access later
  union bchar
  {
    bchar_t chars[8];
    int64_t ival;
  };

  bchar cval;
};

struct StringAST : public ConstExprAST
{
  virtual ~StringAST(){};

  BString sval;
};

typedef std::vector<ConstExprAST*> ConstExprASTList_t;

struct ExtrnVarDeclAST : ExtrnDeclAST
{
  virtual ~ExtrnVarDeclAST() {};
  ConstExprASTList_t vals;
  int vectorAllocation;
};

typedef std::vector<ExtrnDeclAST*> ExtrnDeclASTList_t;

typedef struct BProgramAST
{
  ~BProgramAST()
  {
    for(auto& e : extrnDefs)
      {
	delete e;
      }
  }
    ExtrnDeclASTList_t extrnDefs;
} BProgramAST;



typedef struct BParser
{ 
  inline void init(std::shared_ptr<BLexer> lexer)
  {
    blex = lexer;
    progAST.reset(new BProgramAST());
  }
  
  void genAST(void);
  void parseExtrn(void);
  void parseVarExtrn(ExtrnVarDeclAST* node);
  void parseFuncExtrn(FuncDeclAST* node);
  
  std::vector<BToken>::iterator curToken;
  std::shared_ptr<BLexer> blex;
  std::shared_ptr<BProgramAST> progAST;
}BParser;
