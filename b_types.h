#pragma once

#include <string>
#include <cstdint>

#if defined(BLANG_WIDE_STRING)

#include <cwchar>

typedef wchar_t bchar_t;
typedef std::wstring BString;

#define getbch  fgetwc
#define ungetbch ungetwc
#else

typedef char bchar_t;
typedef std::string BString;

#define getbch fgetc
#define ungetbch ungetc

#endif // defined
