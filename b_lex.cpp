#include "b_lex.h"

void
BLexer::init(FILE* fpIn)
{
    m_context.reset(new BLexContext());
    m_context->m_fpIn = fpIn;
}

bchar_t
BLexer::peekNextChar(void)
{
    //yes this returns an int for EOF
    bchar_t ch = getbch(m_context->m_fpIn);
    ungetbch(ch, m_context->m_fpIn);

    return ch;
}

bchar_t
BLexer::getNextChar(void)
{
    bchar_t ch = getbch(m_context->m_fpIn);

    return ch;
}

//no analysis here, just make tokens to stream to parser
BToken
BLexer::getNextToken(void)
{
    bchar_t ch = peekNextChar();
    BToken tok;

    if(ch == EOF)
    {
        tok.token += ch;
        tok.type = BSymbol::kNull;
        tok.line = ++(m_context->m_lineCount);
        return tok;
    }
    //eat whitespace first
    while(isspace(ch))
    {
        if(ch == '\n')
            m_context->m_lineCount++;

        getNextChar();
        ch = peekNextChar();
    }

    //comments
    if(ch == TOK_DIV)
    {
        bchar_t t = ch;
        getNextChar();
        ch = peekNextChar();

        if(ch == TOK_DIV)
        {
            do
            {
                ch = getNextChar();
            } while(ch != TOK_ENDL);

            ch = peekNextChar();
        }
        else
        {
            ungetbch(t, m_context->m_fpIn);
            ch = peekNextChar();
        }
    }

    if(isalpha(ch) || ch == '_')
    {
        do
        {
            tok.token += ch;
            getNextChar();
            ch = peekNextChar();
            if(tok.token.length() >= MAX_IDENT_LEN)
                break;

        }while(isalnum(ch) || ch == '.' || ch == '_');

        if(tok.token == TOK_IF)
            tok.type = BSymbol::kIf;
        else if(tok.token == TOK_ELSE)
            tok.type = BSymbol::kElse;
        else if(tok.token == TOK_WHILE)
            tok.type = BSymbol::kWhile;
        else if(tok.token == TOK_SWITCH)
            tok.type = BSymbol::kSwitch;
        else if(tok.token == TOK_CASE)
            tok.type = BSymbol::kCase;
        else if(tok.token == TOK_BRK)
            tok.type = BSymbol::kBreak;
        else if(tok.token == TOK_RETURN)
            tok.type = BSymbol::kReturn;
        else if(tok.token == TOK_AUTO)
            tok.type = BSymbol::kAuto;
        else if(tok.token == TOK_EXTRN)
            tok.type = BSymbol::kExtrn;
        else if(tok.token == TOK_DEFAULT)
            tok.type = BSymbol::kDefault;
        else if(tok.token == TOK_GOTO)
            tok.type = BSymbol::kGoto;
        else
            tok.type = BSymbol::kIdent;

        tok.line = m_context->m_lineCount;
        return tok;
    }

    //numbers
    if(isdigit(ch))
    {
        do
        {
            tok.token += ch;
            getNextChar();
            ch = peekNextChar();
        } while(isdigit(ch) || ch == '_');

        tok.type = BSymbol::kWord;
        tok.line = m_context->m_lineCount;
        return tok;
    }

    switch(ch)
    {
    case TOK_STAR:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kMul;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_DIV:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kDiv;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_MOD:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kMod;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_OR:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kOr;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_XOR:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kXor;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_AND:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kAnd;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_BNOT:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kBitNot;
            tok.line = m_context->m_lineCount;
            return tok;
        }

    case TOK_COM:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kComma;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_SEMI:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kSColon;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_LBRC:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kLBrac;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_RBRC:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kRBrac;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_LBRKT:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kLBrak;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_RBRKT:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kRBrak;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_LPAR:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kLParen;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_RPAR:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kRParen;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_TQ:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kQtern;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_TC:
        {
            getNextChar();
            tok.token += ch;
            tok.type = BSymbol::kColon;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_SQUOT:
        {
            getNextChar(); //eat '
            ch = peekNextChar();
            while(ch != EOF && ch != TOK_SQUOT && ch != TOK_ENDL)
            {
                getNextChar();
                tok.token += ch;
                ch = peekNextChar();
            }
            if(ch != TOK_SQUOT)
            {
                 LOG_ERR("ERROR! Missing matching ['] for CHAR token `%s' :: line %d :: token %lu\n", tok.token.c_str(), m_context->m_lineCount, m_context->m_tokQueue.size());
            }
            else
            {
                getNextChar(); //eat '
            }
            tok.type = BSymbol::kChar;
            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_DQUOT:
        {
            getNextChar(); //eat "
            ch = peekNextChar();
            while(ch != EOF && ch != TOK_DQUOT && ch != TOK_ENDL)
            {
                getNextChar();
                tok.token += ch;
                ch = peekNextChar();
            }
            if(ch != TOK_DQUOT)
            {
                 LOG_ERR("ERROR! Missing matching [\"] for STRING token `%s' :: line %d :: token %lu\n", tok.token.c_str(), m_context->m_lineCount, m_context->m_tokQueue.size());
            }
            else
            {
                getNextChar(); //eat "
            }
            tok.type = BSymbol::kString;
            tok.line = m_context->m_lineCount;
            return tok;
        }

    //These are potentially starts of multi-symbol tokens
    case TOK_ASS:
        {
            getNextChar();
            bchar_t p = peekNextChar();

            tok.token += ch;

            if(p == TOK_ASS)
            {
                getNextChar();
                tok.token += p;

                p = peekNextChar();
                if(p == TOK_ASS)
                {
                    getNextChar();
                    tok.token += p;
                    tok.type = BSymbol::kEqAssign;
                }
                else
                {
                    tok.type = BSymbol::kEqual;
                }

                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_GT)
            {
                getNextChar();
                tok.token += p;

                p = peekNextChar();
                if(p == TOK_ASS)
                {
                    getNextChar();
                    tok.token += p;
                    tok.type = BSymbol::kGteAssign;
                }
                else if(p == TOK_GT)
                {
                    getNextChar();
                    tok.token += p;
                    tok.type = BSymbol::kRshAssign;
                }
                else
                {
                    tok.type = BSymbol::kGtAssign;
                }

                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_LT)
            {
                getNextChar();
                tok.token += p;

                p = peekNextChar();
                if(p == TOK_ASS)
                {
                    getNextChar();
                    tok.token += p;
                    tok.type = BSymbol::kLteAssign;
                }
                else if(p == TOK_LT)
                {
                    getNextChar();
                    tok.token += p;
                    tok.type = BSymbol::kLshAssign;
                }
                else
                {
                    tok.type = BSymbol::kLtAssign;
                }

                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_LNOT)
            {
                bchar_t pp = getNextChar(); //!
                p = peekNextChar(); //=?

                //=! is not a valid operator
                if(p != TOK_ASS)
                {
                    ungetbch(pp, m_context->m_fpIn);
                    tok.type = BSymbol::kAssign;
                    tok.line = m_context->m_lineCount;
                    return tok;
                }
                if(p == TOK_ASS)
                {
                    getNextChar();
                    tok.token += pp;
                    tok.token += p;
                    tok.type = BSymbol::kNqAssign;
                    tok.line = m_context->m_lineCount;
                    return tok;
                }
            }

            if(p == TOK_OR)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kOrAssign;
                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_XOR)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kXorAssign;
                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_AND)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kAndAssign;
                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_ADD)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kAddAssign;
                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_MIN)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kSubAssign;
                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_STAR)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kMulAssign;
                tok.line = m_context->m_lineCount;
                return tok;
            }

            if(p == TOK_DIV)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kDivAssign;
                tok.line = m_context->m_lineCount;
                return tok;
            }


            //else
            tok.type = BSymbol::kAssign;
            tok.line = m_context->m_lineCount;
            return tok;
        }

    case TOK_ADD:
        {
            getNextChar();
            bchar_t p = peekNextChar();

            tok.token += ch;

            if(p == TOK_ADD) //++
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kIncr;
            }
            else
            {
                tok.type = BSymbol::kAdd;
            }

            tok.line = m_context->m_lineCount;
            return tok;
        }
    case TOK_MIN:
        {
            getNextChar();
            bchar_t p = peekNextChar();

            tok.token += ch;

            if(p == TOK_MIN)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kDecr;
            }
            else
            {
                tok.type = BSymbol::kSub;
            }

            tok.line = m_context->m_lineCount;
            return tok;
        }

    case TOK_LNOT:
        {
            getNextChar();
            bchar_t p = peekNextChar();
            tok.token += ch;
            if(p == TOK_ASS)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kNotEqual;
            }
            else
            {
                tok.type = BSymbol::kLogNot;
            }

            tok.line = m_context->m_lineCount;
            return tok;
        }

    case TOK_GT:
        {
            getNextChar();
            bchar_t p = peekNextChar();
            tok.token += ch;
            if(p == TOK_ASS)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kGreaterEq;
            }
            else if(p == TOK_GT)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kRshift;
            }
            else
            {
                tok.type = BSymbol::kGreater;
            }

            tok.line = m_context->m_lineCount;
            return tok;
        }

    case TOK_LT:
        {
            getNextChar();
            bchar_t p = peekNextChar();

            tok.token += ch;
            if(p == TOK_ASS )
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kLessEq;
            }
            else if(p == TOK_LT)
            {
                getNextChar();
                tok.token += p;
                tok.type = BSymbol::kLshift;
            }
            else
            {
                tok.type = BSymbol::kLess;
            }

            tok.line = m_context->m_lineCount;
            return tok;
        }

    default:
        LOG_ERR("Bad Symbol [%c] on line %i\n", ch, m_context->m_lineCount);
        getNextChar();
        return getNextToken(); // recurse to next valid?
    }
}

bool
BLexer::genTokens(void)
{
    BToken token;

    do
    {
        token = getNextToken();
        m_context->m_tokQueue.push_back(token);
        LOG_MSG("TOKEN: %s :: type %d :: line %d :: count %lu \n", token.token.c_str(), token.type, token.line, m_context->m_tokQueue.size());
    } while(token.type != BSymbol::kNull);

    LOG_MSG("QUEUE SIZE:(genTokens) %lu\n", m_context->m_tokQueue.size());

    return m_context->m_tokQueue.empty();
}
