#pragma once

#include "b_types.h"

typedef enum    {
    kIf, kElse, kWhile, kSwitch, kCase, kBreak, kDefault, kAuto, kExtrn, kGoto, kReturn,

    kAssign=100, kAddAssign, kSubAssign, kMulAssign, kDivAssign, kModAssign,
    kOrAssign, kAndAssign, kXorAssign, kLshAssign, kRshAssign,
    kLtAssign, kLteAssign, kGtAssign, kGteAssign, kNqAssign, kEqAssign,

    kAdd=200, kSub, kMul, kDiv, kMod, kOr, kAnd, kXor, kLshift, kRshift, kLess,
    kLessEq, kGreater, kGreaterEq, kEqual, kNotEqual, kLogOr, kLogAnd,

    kDecr=300, kIncr, kLogNot, kBitNot, kDeRef, kAddrOf, kNegate,

    kQtern=400, kColon, kSColon, kComma, kLParen, kRParen, kLBrak, kRBrak, kLBrac, kRBrac,

    kIdent=500, kWord, kChar, kString, kHexWord, kSQuot, kDQuot, kNull

} BSymbol;

typedef struct BToken
{
    BToken()
    : token(""),
    type(kNull),
    line(0)
    {}

    BToken(const BToken& rhs)
    {
        token = rhs.token;
        type = rhs.type;
        line = rhs.line;
    }

    BToken& operator=(const BToken& rhs)
    {
        if(this != &rhs)
        {
            token = rhs.token;
            type = rhs.type;
            line = rhs.line;
        }

        return *this;
    }

    BString     token;
    BSymbol     type;
    uint32_t    line;
}BToken;

#define MAX_IDENT_LEN 8

#define TOK_IF  "if"
#define TOK_ELSE "else"
#define TOK_WHILE "while"
#define TOK_SWITCH "switch"
#define TOK_CASE "case"
#define TOK_BRK "break"
#define TOK_DEFAULT "default"
#define TOK_AUTO "auto"
#define TOK_EXTRN "extrn"
#define TOK_GOTO "goto"
#define TOK_RETURN "return"

/*
[a-zA-Z_][a-zA-Z0-9_.]* {   SAVE_STRING; return IDENT; }
[0-9]+                  {   yylval.word = numToST(yytext);
                            return WORD;  }
\"(\\.|[^\\"])*\"       {   SAVE_STRING; puts(yylval.str->c_str()); return STRING; }
\'(\\.|[^\\']){1,8}\'   {   SAVE_STRING; printf("CHAR: %s\n", yylval.str->c_str()); return CHAR;   }
*/
#define TOK_ASSOR "=|"
#define TOK_ASSAND "=&"
#define TOK_ASSXOR "=^"
#define TOK_ASSEQ "==="
#define TOK_ASSNEQ "=!="
#define TOK_ASSLT "=<"
#define TOK_ASSLTE "=<="
#define TOK_ASSGT "=>"
#define TOK_ASSGTE "=>="
#define TOK_ASSLFS "=<<"
#define TOK_ASSRTS "=>>"
#define TOK_ASSADD "=+"
#define TOK_ASSMIN "=-"
#define TOK_ASSMUL "=*"
#define TOK_ASSDIV "=/"
#define TOK_ASSMOD "=%"
#define TOK_INC "++"
#define TOK_DEC "--"
#define TOK_GTE ">="
#define TOK_LFS "<<"
#define TOK_RTS ">>"
#define TOK_LTE "<="
#define TOK_EQU "=="
#define TOK_NEQ "!="
#define TOK_ASS '='
#define TOK_MIN '-' //bin_op minus and un_op negate
#define TOK_LNOT '!'
#define TOK_OR '|' //bit and logical or!
#define TOK_AND '&' //bit and logical and!
#define TOK_XOR '^'
#define TOK_BNOT '~'
#define TOK_LT '<'
#define TOK_GT '>'
#define TOK_ADD '+'
#define TOK_MOD '%'
#define TOK_STAR '*' //bin_op multiply and un_op deref
#define TOK_DIV '/'
#define TOK_TQ '?'
#define TOK_TC ':'
#define TOK_LBRC '{'
#define TOK_RBRC '}'
#define TOK_LBRKT '['
#define TOK_RBRKT ']'
#define TOK_LPAR '('
#define TOK_RPAR ')'
#define TOK_SQUOT '\''
#define TOK_DQUOT '"'
#define TOK_HEXW 'x'
#define TOK_SEMI ';'
#define TOK_COM ','
#define TOK_ENDL '\n'
#define TOK_BCOM "/*"
#define TOK_ECOM "*/"
