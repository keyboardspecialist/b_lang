#pragma once


#include <cstdio>
#include <vector>
#include <queue>
#include <memory>

#include "b_tokens.h"
#include "b_types.h"

#define BLANG_LOGGING
#define BLANG_LOG_LEVEL 1

#include "b_log.h"


typedef struct BLexContext
{
    BLexContext()
    : m_lineCount(0)
    {

    }
    ~BLexContext()
    {

    }

    FILE*   m_fpIn;

    uint32_t m_lineCount;

    BString   m_curToken;
    bchar_t   m_curChar;
    std::vector<BToken>    m_tokHistory;
    std::vector<BToken>     m_tokQueue;
}BLexContext;


typedef struct BLexer
{
    BLexer()
    {
    }

   ~BLexer()
    {

    }

    void init(FILE* fpIn);

    bchar_t peekNextChar(void);
    bchar_t getNextChar(void);

    BToken getNextToken(void);
    bool    isSymbolType(BSymbol symbol);

    bool    genTokens(void);

    std::shared_ptr<BLexContext> m_context;

}BLexer;
